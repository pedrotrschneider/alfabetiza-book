# Modificando o projeto

Na grande maioria das vezes, as únicas modificações que o projeto requerem são relacionadas à exportação das diferentes versões do jogo. Se este for o caso, todas as configurações podem ser facilmente acessadas a partir da tela inicial da Godot, logo após abrir o projeto.

1. Após abrir o projeto, comece clicando no nó de nome ```InitLoader```. 

![Imagem faltando](assets/1-click-node.png)

2. Dessa forma, no menu à direita, novas opções surgirão, sendo essas as relevantes para esta seção:

![Imagem faltando](assets/2-edit-settings.png)

3. Na opção `Login Type` podemos selecionar o tipo de login que queremos utilizar para o jogo.

![Imagem faltando](assets/3-login-type.png)

As opções disponíveis são:

- `No Login`: Sem nenhuma forma de login
- `Local Login`: Login em uma instância do servidor rodando localmente (geralmente utilizada para testes)
- `Server Login`: Login em uma instância rodando em um servidor remotamente (geralmente utilizada para releases oficiais)
- `Guest Login`: Login em uma instância rodando em um servidor remotamente, sem necessidade de ter um login já criado para utilizar (geralmente utilizado em situações em que se quer deixar disponível uma demonstração do jogo publicamente, e ainda coletar os dados de desempenho das pessoas que utilizarem essa versão)


4. Na opção `Server URL` podemos especificar a URL do servidor que queremos utilizar remotamente. Essa será a URL utilizada para fazer as chamadas ao servidor nos tipos de login `Server Login` e `Guest Login`.

![Imagem Faltando](assets/4-server-url.png)

5. Na opção `Local URL` podemos especificar a URL do servidor que queremos utilizar localmente. Essa será a URL utilizada para fazer as chamadas ao servidor no tipo de login `Local Login`.

![Imagem faltando](assets/5-local-url.png)

6. Tenha certeza de salvar as mudanças feitas apertando `ctrl + s`. As mudanças não se tornarão efetivas se as mudanças não forem salvas. Você pode conferir se possui mudanças não salvas checando se há o símbolo `(*)` ao lado do nome da cena no editor da Godot.

![Imagem faltando](assets/6-saving-icon.png)

A imagem acima mostra um exemplo onde há mundaças que não foram salvas ainda. Ao salvar com `ctrl + s`, o símbolo `(*)` irá desaparecer.