# Exportando para web (HTML5)

3. Clique no item que diz ```HTML5 (Runnable)``` e clique no botão ```Export Project```.

![Imagem Faltando](assets/15-html-export-preset.png)

4. Se abrirá uma janela de explorador de arquivos. Navegue até a pasta em que deseja ter os arquivos da build de web e clique no botão ```Save```. (Caso a caixa de seleção que diz ```Export With Debug``` esteja marcada, desmarque ela antes de clicar em salvar).

![Imagem Faltando](assets/16-html-select-path.png)

Se tudo correr bem, a janela irá se fechar e os arquivos HTML/JavaScript/CSS estarão prontos na pasta selecionada.