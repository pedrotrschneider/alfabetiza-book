# Baixando os Export Templates

Para ser possível exportar o projeto da Godot para várias plataformas, são necessários os ```Export Templates```. Para baixá-los, siga esses passos:

1. Clique no botão ```Editor``` no canto superior esquerdo

![Botão Editor](assets/7-editor-button.png)

2. Clique no botão ```Manage Export Templates```

![Botão Manage Export Templates](assets/8-manage-templates-button.png)

3. Na janela que aparecer, clique no botão ```Download and Install```

![Botão Download and Install](assets/9-download-and-install-button.png)

4. Irá se abrir uma nova janela com uma barra de progresso. Basta esperar os arquivos terminarem de serem baixados.

![Tela de Download](assets/10-downloading-screen.png)

5. Quando o download terminar, basta clicar no botão ```Close``` e estará tudo certo.

![Botão de Close](assets/11-close-button.png)

Caso queira saber onde esses arquivos foram colocados, clique no botão ```Editor``` e depois clique no botão ```Open Editor Data Folder``` e um explorador de arquivos irá se abrir na pasta. Os export templates foram baixados dentro da pasta ```templates```.