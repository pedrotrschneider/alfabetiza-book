# 0.2.23.01.11.0

Nessa versão, a maioria das mudanças foram voltadas para melhorias estéticas no jogo. Para isso, foram feitos novos estilos para os elementos de interface seguindo padrões neumorphicos para a estilização; além de ter sido feito o plano de fundo do elemento Ar, que estava faltando. Por fim, foi adicionado suporte e builds funcionais para android.

## Mudanças notáveis nessa versão

- Muda o estilo dos elementos de interface do jogo, passando a seguir um estilo neumorfico.
![Imagem do design inicial da tela de agradecimentos](assets/img1.png)

- Cria e adiciona no jogo o plano de fundo do elemento Ar que estava faltando.
![Imagem do design inicial da tela de agradecimentos](assets/img2.png)

- Adiciona animais ajudantes diferentes nos elementos diferentes.
![Imagem da Cutia ajudante](assets/tutorial1.png)
![Imagem da Cutia ajudante](assets/tutorial2.png)
![Imagem da Cutia ajudante](assets/tutorial3.png)
![Imagem da Cutia ajudante](assets/tutorial4.png)

- Adiciona sons de ambiente, que ficam tocando no fundo de cada elemento.

- Adiciona suporte e builds para android.