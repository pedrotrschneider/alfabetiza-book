# Exportando as builds

Além dos ```Export Templates```, no repositório clonado temos também os ```Export Presets```, que contém as informações necessárias para exportas especificamente o projeto Alfabetiza. Usando esses presets, exportar para qualquer plataforma fica muito fácil.

Para qualquer plataforma, o começo do processo é o mesmo:

1. Depois de abrir o projeto, clique no botão project.

![Imagem Faltando](assets/12-project-button.png)

2. Clique no botão export

![Imagem Faltando](assets/13.export-button.png)

Se abrirá a janela de selecão de ```Export Presets```.

![Imagem Faltando](assets/14-export-presets-screen.png)

Agora, siga os passos específicos de cada plataforma.