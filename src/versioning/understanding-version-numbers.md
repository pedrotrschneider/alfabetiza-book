# Entendendo os números das versão

Esse documento se dedica a explicar o sistema de numeração de versões que eu adotei para o projeto.

## Primeiro número - 0.x.xx.xx.xx.x

O primeiro número representa as versões majoritárias do projeto. Uma mudança no primeiro dígito do número de versão representa uma grande mudança na forma como o aplicativo funciona, ou a finalização de uma parte importante do projeto.

Por exemplo, se o primeiro número da versão é ```0```, isso significa que o projeto ainda não chegou ao ponto em que eu estou satisfeito de chamar de "primeira versão", ou seja, a visão inicial do aplicativo ainda não foi realizada. Quando isso ocorrer, o primeiro número da versão mudará para ```1```. Se, um dia, ocorrer uma mudança muito grande na forma como o jogo funciona, ou for adicionada uma grande quantidade de novas funcionalidades, o primeiro número da versão irá mudar para ```2```.

## Segundo número - x.0.xx.xx.xx.x

O segunda número representa mudanças menores de versão que o primeiro número. Se ocorrer alguma mudança significativa ou o projeto chegar em algum ponto crucial, mas que não for grande o suficiente para causar uma mudança no primeiro número de versão, é o segundo número que irá mudar.

Por exemplo, se o segundo número da versão é o ```2```, uma mudança que pode causar este número a subir para ```3``` é a finalização completa de algum elemento ou funcionalidade grande.

## Os três próximos números - x.x.22.12.05.x

Esse conjunto de 3 números é utilizado para sinalizar o dia em que a versão foi liberada. No entanto, uma mudança nesses números não necessariamente representa uma mudança no primeiro ou no segundo números da versão.

Por exemplo, se o número da versão é o ```0.0.22.12.05.0```, isso significa que essa versão foi liberada no dia ```5 de dezembro de 2022```.

## O último número - x.x.xx.xx.xx.0

O último número só é utilizado se mais de uma versão for liberada no mesmo dia. Isso pode ocorrer caso eu perceba que alguma versão possui um problema que precisa ser resolvido imediatamente.

Por exemplo, se eu liberar uam versão de número ```0.0.22.12.05.0``` e, ainda no mesmo dia, perceber algum problema que precisa ser resolvido imediatamente, eu farei uma nova versão de número ```0.0.22.12.05.1```.