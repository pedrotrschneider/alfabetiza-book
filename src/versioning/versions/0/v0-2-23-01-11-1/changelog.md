# 0.2.23.01.11.1

Essa versão apenas corrige alguns bugs e problemas da versão anterior. Mais especificamente, problemas na build de Android com as cartas que devem ser arrastadas, e com problemas de permissão no Android.