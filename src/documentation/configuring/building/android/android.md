# Exportando para Android

Primeiro, é necessário instalar as dependências do sistema de build Android no computador. Para isso, siga os passos na [documentação oficial da Godot](https://docs.godotengine.org/pt_BR/latest/tutorials/export/exporting_for_android.html).

Depois de seguir todos os passos como descrito, será possível exportar para Android seguindo os mesmos passos que para qualquer outra plataforma:

```
Project Settings > Export... > Android > Export Project
```

Note que para exportar para Android em modo release, é preciso gerar em um segundo keystore file e adicionar no export template, como descrito na documentação da Godot.