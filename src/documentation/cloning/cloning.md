# Clonando o projeto

A primeira coisa a fazer é clonar o repositório do projeto em sua máquina. Para isso, basta acessar [este link](https://gitlab.com/pedrotrschneider/alfabetiza-godot), clicar no botão azul escrito ```Clone``` e clicar em ```Download zip```. Depois, basta extrair o arquivo ```.zip``` em algum lugar em seus arquivos.

Alternativamente, é possível apenas rodar o seguinte comando para clonar com ```git```:

```
git clone https://gitlab.com/pedrotrschneider/alfabetiza-godot.git
```