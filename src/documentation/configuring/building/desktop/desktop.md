# Exportando para desktop (Windows/Linux)

O processo será o mesmo para gerar builds de Linux ou Windows.

3. Clique no item que diz ```Linux/X11 (Runnable)```, ou ```Windows Desktop (Runnable)```

![Imagem Faltando](assets/17-desktop-export-preset.png)

4. Se abrirá uma janela de explorador de arquivos. Navegue até a pasta em que deseja ter os arquivos da build de desktop e clique no botão ```Save```. (Caso a caixa de seleção que diz ```Export With Debug``` esteja marcada, desmarque ela antes de clicar em salvar).

![Imagem Faltando](assets/18-desktop-select-path.png)

Se tudo correr bem, haverá um executável (```.x86_64``` para Linux, ou ```.exe``` para Windows) na pasta que foi selecionada durante o processo.