# **Exportando o projeto para diversas plataformas**

## **Sumário**

- [Clonando o projeto](#clonando-o-projeto)
- [Configurando a Godot](#configurando-a-godot)
    - [Executável](#executável)
    - [Importando o projeto](#importando-o-projeto)
    - [Baixando o plugin Dialogic](#baixando-o-plugin-dialogic)
    - [Baixando os Export Templates](#baixando-os-export-templates)
    - [Exportando as Builds](#exportando-as-builds)
        - [Web](#web)
        - [Desktop](#desktop)
        - [Android](#android)
- [Modificando o projeto](#modificando-o-projeto)
    - [Mudando o caminho do servidor](#mudando-o-caminho-do-servidor)
    - [Habilitando e desabilitando o login](#habilitando-e-desabilitando-o-login)


## **Configurando a Godot**

## **Modificando o projeto**

### **Mudando o caminho do servidor**

Para mudar a URL das chamadas do servidor, basta modificar uma linha em um arquivo de texto. Estando na pasta raíz do repositório, navegue para a pasta

```
project/http_requester/
```

Dentro desta pasta, há um arquivo chamado ```HTTPRequester.gd``` (tem dois arquivos de nome parecido, um com a extensão ```.gd```, e outro com a extensão ```.tscn```; o que deve ser modificado é o que tem a extensão ```.gd```). Abra este arquivo; o começo dele deve ser parecido com:

```python
extends Control

signal request_completed(response_code, body);

#const HOST_URL : String = "http://localhost/alfabetiza/";
const HOST_URL : String = "http://www.usp.br/line/alfabetiza/jogo/server/";
const LOGIN_URL : String = "login.php";
const MINIGAME_DATA_URL : String = "minigame_data.php";

...
```

A constante ```HOST_URL``` guarda a URL da raíz do servidor.

A constante ```LOGIN_URL``` guarda a URL (relativa à ```HOST_URL```) das requisições de login.

A constante ```MINIGAME_DATA_URL``` guarda a URL (relativa à ```HOST_URL```) das requisições de dados de minijogos.

Essas variáveis podem ser modificadas a depender da estrutura do servidor utilizado.

Caso deseje que o jogo rode na versão local de seu servidor, basta descomentar a linha 

```python
const HOST_URL : String = "http://localhost/alfabetiza/";
```

e deixar comentada a linha

```python
const HOST_URL : String = "http://www.usp.br/line/alfabetiza/jogo/server/";
```

Caso deseje que o jogo rode na versão do servidor da usp, realizar o processo inverso, comentando a primeira linha e descomentando a segunda.

Para comentar uma linha em ```GDScript``` basta acrescentar um ```#``` ao começo da linha. Para descomentar, basta remover o ```#``` do começo da linha, como se fosse ```Python```.

### **Habilitando e desabilitando o login**

Para fazer isso, basta mudar a cena inicial que a Godot executa. Para isso:

1. Clique em ```Project``` no canto superior esquerdo

![Botão Project](resources/login-01.png)

2. Clique em ```Project Settings```

![Botão Project Settings](resources/login-02.png)

3. Clique em ```Run```

![Botão Run](resources/login-03.png)

4. No campo ```Main Scene``` clique no ícone de pasta para selecionar a cena inicial

![Botão da pasta](resources/login-04.png)

Caso queira que o loigin seja habilitado, selecione a cena ```LoginScreen.tscn``` no caminho ```res://loign_screen``` e clique em ```open```

![Ativando login](resources/login-06.png)

Caso queira que o login seja desabilitado, selecione a cena ```ElementSelector.tscn``` no caminho ```res://elements``` e clique em ```open```

![Desativando login](resources/login-05.png)
