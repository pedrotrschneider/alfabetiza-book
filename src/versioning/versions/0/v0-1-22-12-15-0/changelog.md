# 0.1.22.12.15.0

Nessa versão, a maioria das mudanças foram relacionadas ao sistema de build, de forma a facilitar o processo de criação dos executáveis das diferentes versões do app. A documentação também será atualizada para refletir estas mudanças no processo de build.

## Mudanças notáveis nessa versão

- Adiciona uma tela com agradecimentos e créditos aos softwares que foram majoritariamente utilizados na produção do jogo, além de dar crédito aos criadores dos áudios que foram utilizados no elemento ar.

![Imagem do design inicial da tela de agradecimentos](assets/credits-screen-initial.png)

- Simplifica o processo de build das diferentes versões do jogo. Agora, todas as opções de build podem ser acessadas por meio da página inicial da Godot após abrir o projeto.

- Retira algumas bibliotecas que eram carregadas de terceiros das builds de web. Agora, todas as dependências da versão web estão presentes localmente.

## Próximos passos

- Fazer o plano de fundo do elemento Ar.

- Mudar o design de seleção dos minijogos.

- Mudar o design de seleção de elementos.