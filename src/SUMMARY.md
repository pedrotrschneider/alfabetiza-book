# Summary

[Alfabetiza](README.md)

- [Documentação](documentation/documentation-description.md)

    - [Clonando o projeto](documentation/cloning/cloning.md)

    - [Configurando a Godot](documentation/configuring/configuring.md)

        - [Instalando a Godot](documentation/configuring/downloading-godot/downloading-goodt.md)

        - [Importando o projeto na Godot](documentation/configuring/importing/importing.md)

        - [Baixando o plugin Dialogic](documentation/configuring/dialogic/dialogic.md)

        - [Baixando os Export Templates](documentation/configuring/export-templates/export-templates.md)

        - [Exportando as builds](documentation/configuring/building/building.md)

            - [Web](documentation/configuring/building/web/web.md)

            - [Desktop](documentation/configuring/building/desktop/desktop.md)

            - [Android](documentation/configuring/building/android/android.md)
    
    - [Modificando o projeto](documentation/modifying/modifying.md)

- [Changelog das versões](versioning/changelog-description.md)

    - [Entendendo os números das versões](versioning/understanding-version-numbers.md)

    - [Versão 0](versioning/versions/version-0.md)

        - [v0.2.23.01.11.1](versioning/versions/0/v0-2-23-01-11-1/changelog.md)

        - [v0.2.23.01.11.0](versioning/versions/0/v0-2-23-01-11-0/changelog.md)

        - [v0.1.22.12.16.0](versioning/versions/0/v0-1-22-12-16-0/changelog.md)

        - [v0.1.22.12.15.0](versioning/versions/0/v0-1-22-12-15-0/changelog.md)
        
        - [v0.0.22.12.05.0](versioning/versions/0/v0-0-22-12-05-0/changelog.md)
