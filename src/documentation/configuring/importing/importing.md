# Importando o projeto na Godot

Tendo o executável da Godot, o próximo passo é importar o projeto para acessar a engine:

1. Abra a godot.

2. Se aparecer algum pop-up falando que você não possui nenhum projeto, clique no botão ```Cancel```.

![Project Manager da Godot](assets/1-cancel-button.png)

3. No canto direito, no meio, clique no botão ```Import```.

![Project Manager da Godot](assets/2-import-button.png)

4. Um pop-up pedindo o caminho do projeto irá aparecer. Clique no botão ```Browse```.

![Botão Browse](assets/3-browse-button.png)

5. Navegue para o local onde o projeto foi clonado anteriormente, entre na pasta ```project``` e selecione o arquivo ```project.godot``` dando dois cliques nele, ou clicando no botão ```open``` após selecionar.

![Arquivo e botão de abrir arquivo](assets/4-file-open-button.png)

6. Fazendo isso, o explorador de arquivos deve fechar, e um ícone verde deve aparecer ao lado do botão ```Browse```. Se esse for o caso, clique no botão ```Import & Edit```.

![Botão Import & Edit](assets/5-import-edit-button.png)

7. Se tudo der certo, você verá essa tela:

![Tela inicial](assets/6-main-screen.png)

E assim, o projeto está importado! Das próximas vezes que você abrir a Godot, o projeto já estará na tela inicial da lista de projetos e poderá ser aberto por lá.