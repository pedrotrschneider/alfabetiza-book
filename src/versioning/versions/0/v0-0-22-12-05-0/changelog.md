# 0.0.22.12.05.0

Essa é a primeira versão oficialmente documentada do projeto, mas já houveram diversas outras versões antes dessa.

O começo do desenvolvimento dessa versão se deu num ponto em que todos os minijogos e artes para os minijogos haviam sido finalizados, faltando apenas o plano de fundo do elemento ar.

## Mudanças notáveis nessa versão

- Muda o design das cenas de leitura da lenda, aumentando a fonte e adicionando a ilustração de um livro:

![Imagem da lenda do elemento terra](assets/earth-tale.png)

![Imagem da lenda do elemento água](assets/water-tale.png)

![Imagem da lenda do elemento fogo](assets/fire-tale.png)

- Otimiza o plano de fundo do elemento fogo, melhorando a performance em aparelhos menos potentes.

- Faz as cartas que tocam áduio (como as do Palavra Valise 2 e do Acrofonia) toquem o áudio quando o mouse passa por cima da carta ao invés de esperar o clique.

- Faz com que o próximo nível seja automaticamente carregado ao finalizar um nível.

## Próximos passos

- Fazer o plano de fundo do elemento Ar.

- Simplificar o processo de exportação das diferentes versões do jogo (para as diferentes plataformas, sem login, login local e login pelo servidor).

- Mudar o design de seleção dos minijogos.

- Mudar o design de seleção de elementos.