# Baixando o plugin Dialogic

A Godot é distribuída de forma modular para que o executável não fique muito grande. Por isso, há um série de plugins disponíveis a serem usados, alguns oficiais e outros feitos pela comunidade.

O projeto Alfabetiza utiliza um desses plugins, chamado Dialogic, para lidar com o sistema de diálogos do jogo. Ele precisa ser instalado para que as exportações corram bem.

1. Clique no botão ```AssetLib``` no canto superior central da tela

![Botão Asset Lib](assets/dialogic-01.png)

2. Digite ```dialogic``` na barra de busca e clique na opção de nome ```Dialogic - Dialogue Editor```

![Buscando por Dialogic](assets/dialogic-02.png)

3. Na nova janela que se abrirá, clique em ```Dowload```

![Botão Download](assets/dialogic-03.png)

4. Após o download ser finalizado, uma nova janela se abrirá mostrando os arquivos que foram baixados. Clique em ```Install```

![Botão Install](assets/dialogic-04.png)

5. Espere o plugin ser instalado

![Esperando instalação](assets/dialogic-05.png)

6. Se tudo der certo, deve aparecer uma janela falando sobre o sucesso da instalação. Clique em ```Ok```

![Botão Ok](assets/dialogic-06.png)

7. Agora, precisamos ativar o plugin. Clique no botão ```Project``` no canto superior esquerdo

![Botão Project](assets/dialogic-07.png)

8. Clique em ```Project Settings```

![Botão Project Settings](assets/dialogic-08.png)

9. Clique em ```Plugins```

![Botão Plugins](assets/dialogic-09.png)

10. Ative o plugin do ```Dialogic``` clicando na caixa ao lado da palavra ```Enabled``` (ela deve ficar marcada e azul como na imagem abaixo)

![Ativando Dialogic](assets/dialogic-10.png)

Com isso, o plugin foi instalado e ativado corretamente!